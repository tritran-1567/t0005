<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<div class="c-banner">
    <img src="./assets/image/bn1-pc.png" alt="" class="c-banner__bn1 pc-only">
    <img src="./assets/image/bn1-sp.png" alt="" class="c-banner__bn1 sp-only">
    <img src="./assets/image/bn2-sp.png" alt="" class="c-banner__bn2 sp-only">
</div>
<nav class="c-navi1 pc-only">
    <ul>
        <li>
            <a href="#">
                <img src="./assets/image/arrow1-pc.png" alt="">
                キャンペーン情報
            </a>
        </li>
        <li class="u-ml37">
            <a href="#">
                <img src="./assets/image/arrow1-pc.png" alt="">
                ご利用料金
            </a>
        </li>
        <li class="u-ml37">
            <a href="#">
                <img src="./assets/image/arrow1-pc.png" alt="">
                ご予約
            </a>
        </li>
        <li class="u-ml37">
            <a href="#">
                <img src="./assets/image/arrow1-pc.png" alt="">
                講師紹介
            </a>
        </li>
        <li>
            <a href="#">
                <img src="./assets/image/arrow1-pc.png" alt="">
                店舗のご案内
            </a>
        </li>
    </ul>
</nav>
<!-- main -->
<!-- .p-golf1 -->
<section class="p-golf1">
    <div class="c-campaign">
        <img src="./assets/image/camp1-pc.png" alt="" class="pc-only">
        <img src="./assets/image/camp1-sp.png" alt="" class="sp-only" width="320">
        <div class="c-campaign__sp">
            <p>
                
            </p>
        </div>
    </div>
    <div class="c-list1 pc-only">
        <div class="c-list1__item c-list1__item--bg1 c-list1__item--ol1 pc-only">
            <img src="/assets/image/number1-pc.png" alt="" class="c-list1__no">
            <img src="./assets/image/txt1-pc.png" alt="" class="c-list1__text1">
            <img src="./assets/image/txt4-pc.png" alt="" class="c-list1__text2">
            <img src="./assets/image/txt6-pc.png" alt="" class="c-list1__text3">
            <img src="./assets/image/star1-pc.png" alt="" class="c-list1__star c-list1__star--1">
            <img src="./assets/image/star2-pc.png" alt="" class="c-list1__star c-list1__star--2">
            <img src="./assets/image/star1-pc.png" alt="" class="c-list1__star c-list1__star--3">
            <img src="./assets/image/star2-pc.png" alt="" class="c-list1__star c-list1__star--4">
            <div class="c-list1__box"></div>
        </div>
        <div class="c-list1__item c-list1__item--bsd1 pc-only">
            <img src="/assets/image/number2-pc.png" alt="" class="c-list1__no">
            <img src="./assets/image/txt2-pc.png" alt="" class="c-list1__text1">
            <br/>
            <img src="./assets/image/txt5-pc.png" alt="" class="c-list1__text2">
            <img src="./assets/image/txt7-pc.png" alt="" class="c-list1__text3">
            <div class="c-list1__box"></div>
        </div>
        <div class="c-list1__item c-list1__item--bsd1 pc-only">
            <img src="/assets/image/number3-pc.png" alt="" class="c-list1__no">
            <img src="./assets/image/txt3-pc.png" alt="" class="c-list1__text1">
            <img src="./assets/image/txt5-pc.png" alt="" class="c-list1__text2">
            <br/>
            <img src="./assets/image/txt8-pc.png" alt="" class="c-list1__text3">
            <div class="c-list1__box"></div>
        </div>
    </div>
    <div class="c-list3__item sp-only"><img src="./assets/image/box1-sp.png" alt="" width="302"></div>
    <div class="c-list3 sp-only">
        <div class="c-list3__item"><img src="./assets/image/box2-sp.png" alt="" width="150"></div>
        <div class="c-list3__item"><img src="./assets/image/box3-sp.png" alt="" width="150"></div>
    </div>
    <div class="c-btn1">
        <a href="#">
            <img src="./assets/image/arrow2-pc.png" alt="" class="pc-only">
            <img src="./assets/image/arrow2-sp.png" alt="" class="sp-only" width="8">
            見学・体験レッスンのご予約はこちら
        </a>
    </div>
</section>
<!-- .p-golf2 -->
<section class="p-golf2">
    <div class="c-title1">
        <img src="./assets/image/title1-pc.png" alt="" class="pc-only">
        <img src="./assets/image/title1-sp.png" alt="" class="sp-only" width="189.5">
    </div>
    <div class="c-table1">
        <div class="c-table1__title">
            事前申し込み＜期間限定＞
        </div>
        <div class="c-table1__inner">
            <div class="c-table1__item">
                <ul>
                    <li>
                        体験レッスン
                    </li>
                    <li>
                        <img src="./assets/image/price1-pc.png" alt="" class="pc-only">
                        <img src="./assets/image/price1-sp.png" alt="" class="sp-only u-l4">
                    </li>
                    <li>
                        <img src="./assets/image/price3-pc.png" alt="" class="pc-only">
                        <img src="./assets/image/price3-sp.png" alt="" class="sp-only u-l-1">
                    </li>
                </ul>
            </div>
            <div class="c-table1__item">
                <ul>
                    <li>
                        フリー練習
                    </li>
                    <li>
                        <img src="./assets/image/price2-pc.png" alt="" class="pc-only">
                        <img src="./assets/image/price2-sp.png" alt="" class="sp-only u-l-4">
                    </li>
                    <li>
                        <img src="./assets/image/price3-pc.png" alt="" class="pc-only">
                        <img src="./assets/image/price3-sp.png" alt="" class="sp-only u-l-9">
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="c-table2">
        <div class="c-table2__title">
            通常料金 ※(税抜)
        </div>
        <table class="pc-only">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>会員</th>
                    <th>ビジター</th>
                    <th>フリー練習費</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>⼊会⾦</td>
                    <td></td>
                    <td>￥5,000／1回</td>
                    <td>−</td>
                    <td>−</td>
                </tr>
                <tr>
                    <td>レギュラー</td>
                    <td>全⽇利⽤可</td>
                    <td>￥10,000（税抜）／⽉額</td>
                    <td>−</td>
                    <td class="u-c1 u-bold">無料</td>
                </tr>
                <tr>
                    <td>デイタイム</td>
                    <td>平⽇昼限定（11:00〜17:00</td>
                    <td>￥7,000（税抜）／⽉額</td>
                    <td>−</td>
                    <td>時間外有料</td>
                </tr>
                <tr>
                    <td>ジュニア</td>
                    <td class="u-lh19">
                        ⽊曜⽇限定 17：00〜18：00<br/>
                        中学3年生まで
                    </td>
                    <td>￥6,000（税抜）／⽉額</td>
                    <td>−</td>
                    <td>時間外有料</td>
                </tr>
                <tr>
                    <td>ホリデイ</td>
                    <td>⼟・⽇・祝のみ終⽇利⽤可</td>
                    <td>￥8,000（税抜）／⽉額</td>
                    <td>−</td>
                    <td>時間外有料</td>
                </tr>
                <tr>
                    <td>レッスン4</td>
                    <td class="u-lh20">
                        レッスン4回<br/>
                        （スタンプカード制／有効期間：3ヶ月）
                    </td>
                    <td>−</td>
                    <td>￥8,000（税抜）／1セット</td>
                    <td>有料</td>
                </tr>
                <tr class="u-lh21">
                    <td>フリー練習</td>
                    <td>
                        空打席があれば利⽤可<br/>
                        ※レッスンは含まず
                    </td>
                    <td>
                        <span class="c-line1">￥1,000（税抜）／1回</span><br/>
                        <span class="u-c1">プレオープン期間限定 ￥0</span>
                    </td>
                    <td>
                        <span class="c-line1">￥1,500（税抜）／1回</span><br/>
                        <span class="u-c1">プレオープン期間限定 ￥0</span>
                    </td>
                    <td>有料</td>
                </tr>
                <tr>
                    <td>体験レッスン</td>
                    <td>全⽇利⽤可</td>
                    <td>−</td>
                    <td>￥2,000（税抜）／1回</td>
                    <td>−</td>
                </tr>
            </tbody>
        </table>
        <div class="c-box sp-only active">
            <div class="c-box__title">
                入会金
            </div>
            <div class="c-box__inner">
                <div class="c-box__content">
                    <div class="c-box__left">
                        ⼊会⾦
                    </div>
                    <div class="c-box__right">
                        ￥5,000（税抜）／1回
                    </div>
                </div>
            </div>
        </div>
        <div class="c-box sp-only active">
            <div class="c-box__title">
                レギュラー
                <span>全⽇利⽤可</span>
            </div>
            <div class="c-box__inner">
                <div class="c-box__content">
                    <div class="c-box__left">
                        会員
                    </div>
                    <div class="c-box__right">
                        ￥10,000（税抜）／⽉額
                    </div>
                </div>
                <div class="c-box__content">
                    <div class="c-box__left">
                        ビジター
                    </div>
                    <div class="c-box__right">
                        -
                    </div>
                </div>
                <div class="c-box__content">
                    <div class="c-box__left">
                        フリー練習費
                    </div>
                    <div class="c-box__right">
                        無料
                    </div>
                </div>
                <div class="c-box__content">
                    <div class="c-box__left">
                        打席料・ボール代
                    </div>
                    <div class="c-box__right">
                        無料
                    </div>
                </div>
            </div>
        </div>
        <div class="c-box sp-only">
            <div class="c-box__title">
                <span>デイタイム</span>
                <span>平⽇昼限定（11:00〜17:00）</span>
            </div>
        </div>
        <div class="c-box sp-only">
            <div class="c-box__title">
                <span>ジュニア</span>
                <span>
                    ⽊曜⽇限定 17：00〜18：00<br/>
                    中学3年生まで
                </span>
            </div>
        </div>
        <div class="c-box sp-only">
            <div class="c-box__title">
                ホリデイ
                <span>⼟・⽇・祝のみ終⽇利⽤可</span>
            </div>
        </div>
        <div class="c-box sp-only">
            <div class="c-box__title">
                レッスン4
                <span>
                    レッスン4回（スタンプカード制<br/>
                    ／有効期間：3ヶ月）
                </span>
            </div>
        </div>
        <div class="c-box sp-only">
            <div class="c-box__title">
                フリー練習
                <span>
                    空打席があれば利⽤可<br/>
                    ※レッスンは含まず
                </span>
            </div>
        </div>
        <div class="c-box sp-only">
            <div class="c-box__title">
                体験レッスン
                <span>全⽇利⽤可</span>
            </div>
        </div>
    </div>
    <span class="c-promo">
        <span class="c-text1"> 
            レッスン・フリー利用可能時間
        </span>
        1時間（準備時間含む、会員・ビジター問わず）
        <span class="c-text2">
            学生割引
        </span>
        <p>
            上記料金より50％OFF（会員カテゴリのみ適用※フリー練習含まず）
        </p>
    </span>
</section>
<!-- .p-golf3 -->
<img src="./assets/image/gra1-sp.png" alt="" class="c-order__imgTOP sp-only">
<section class="p-golf3">
    <div class="c-order">
        <div class="c-order__title">
            <p class="u-fz32">見学・体験のご予約もこちらから!</p> 
            <img src="./assets/image/txt9-pc.png" alt="" class="pc-only"> 
            <img src="./assets/image/txt9-sp.png" alt="" class="sp-only" width="172"><br class="sp-only"/> 
            レッスンWeb予約
        </div>
        <div class="c-order__subtitle">
            当スクールは予約制となっております。<br/>
            お電話、または予約システムよりご予約をお願い致します。
        </div>
        <div class="c-order__btn">
            <ul>
                <li>
                    <a href="#" class="c-btn2 u-fz21">
                        <img src="./assets/image/icon7-pc.png" alt="" class="pc-only">
                        <img src="./assets/image/icon7-sp.png" alt="" class="sp-only">
                        ご予約はこちら
                    </a>
                </li>
                <li>
                    <a href="tel:0493238015" class="c-btn2 u-fz15">
                        <span>お電話でのご予約</span><br/>
                        <img src="./assets/image/icon8-pc.png" alt="" class="pc-only">
                        <img src="./assets/image/icon8-pc.png" alt="" class="sp-only">
                    </a>
                    <div class="c-order_cal u-fz13">
                        平日11:00～21:00（昼休憩13:00～14:00）／<br/>
                        土・日・祝10:00～19:00<br/>
                        定休日 火曜日<br/>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- pc -->
    <img src="./assets/image/gra1-pc.png" alt="" class="c-order__imgTOP pc-only">
    <img src="./assets/image/gra2-pc.png" alt="" class="c-order__imgBOT pc-only">
    <!-- sp -->
</section>
<img src="./assets/image/gra2-sp.png" alt="" class="c-order__imgBOT sp-only">
<!-- .p-golf4 -->
<section class="p-golf4">
    <div class="c-title1">
        <img src="./assets/image/title2-pc.png" alt="" class="pc-only">
        <img src="./assets/image/title2-sp.png" alt="" class="sp-only" width="288">
    </div>
    <div class="c-instructor">
        <img src="./assets/image/avt1-pc.jpg" alt="" class="pc-only">
        <img src="./assets/image/avt1-sp.jpg" alt="" class="sp-only" width="136">
        <div class="c-instructor__content">
            <div class="c-instructor__title">
                <span class="pc-only">矢島　嘉彦</span>
                <span class="sp-only">講師名が入ります</span>
            </div>
            <div class="c-instructor__text1">
                1,000人以上のレッスン経験があり、初心者から上級者までそれぞれの悩みや疑問を解決し指
                導します。
            </div>
            <ul>
                <li>
                    公益社団法人日本プロゴルフ協会
                </li>
                <li>
                    高校生からゴルフを始め、2006年にティーチングプロとして社団法人日本プロゴル<br class="pc-only"/>
                    フ協会の会員となる
                </li>
            </ul>
        </div>
    </div>
</section>
<!-- .p-golf5 -->
<section class="p-golf5">
    <div class="c-title1">
        <img src="./assets/image/title3-pc.png" alt="" class="pc-only">
        <img src="./assets/image/title3-sp.png" alt="" width="286.5" class="sp-only">
    </div>
    <div class="c-list2">
        <?php for($i = 1; $i <= 5; $i++)  { ?>
        <div class="c-list2__item">
            <a href="#">
                <img src="./assets/image/shop<?php echo $i ?>-pc.jpg" alt="" class="pc-only">
                <img src="./assets/image/shop<?php echo $i ?>-sp.jpg" alt="" class="sp-only" width="136">
                <span>ダミー画像</span>
            </a>
        </div>
        <?php } ?>
    </div>
</section>
<!-- .p-golf6 -->
<section class="p-golf6">
    <div class="c-contact">
        <div class="c-contact__title">
            アクセスマップ
        </div>
        <div class="c-contact__inner">
            <div class="c-contact__map">
                <iframe width="100%" height="100%" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=%20%E5%9F%BC%E7%8E%89%E7%9C%8C%E6%9D%B1%E6%9D%BE%E5%B1%B1%E5%B8%82%E7%AE%AD%E5%BC%93%E7%94%BA1-13-16%20%E5%AE%89%E7%A6%8F%E3%83%93%E3%83%AB1F+(1%206)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=A&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Embed Google Map</a></iframe>
            </div>
            <div class="c-contact__box u-fz15">
                <div class="c-contact__item1">
                    <p>営業時間</p>
                    <p>
                        平日11:00〜21:00(昼休憩13:00〜14:00)<br/>
                        土・日・祝10:00〜19:00
                    </p>
                    <img src="./assets/image/icon2-pc.png" alt="" class="pc-only">
                </div>
                <div class="c-contact__item1">
                    <p>定休日</p>
                    <p>火曜日</p>
                    <img src="./assets/image/icon3-pc.png" alt="" class="pc-only">
                </div>
                <div class="c-contact__item1">
                    <p>
                        レッスン・フリー<br class="pc-only"/>
                        利用可能時間
                    </p>
                    <p>
                        会員・ビジター問わず1時間(準備時間含む)
                    </p>
                    <img src="./assets/image/icon4-pc.png" alt="" class="pc-only">
                </div>
                <div class="c-contact__item1">
                    <p>TEL</p>
                    <p>0493-23-8015</p>
                    <img src="./assets/image/icon5-pc.png" alt="" class="pc-only">
                </div>
                <div class="c-contact__item1">
                    <p>住所</p>
                    <p>
                        〒355-0028<br/>
                        埼玉県東松山市箭弓町1-13-16 安福ビル1F
                    </p>
                    <img src="./assets/image/icon6-pc.png" alt="" class="pc-only">
                </div>
            </div>
            <img src="./assets/image/shadow1-pc.png" alt="" class="c-contact__bg pc-only">
        </div>
    </div>
</section>
<!-- .p-golf7 -->
<section class="p-golf7">
    <div class="c-information">
        <div class="c-information__title">
            <img src="./assets/image/title5-pc.png" alt="" class="pc-only">
            <img src="./assets/image/title5-sp.png" alt="" width="170.5" class="sp-only">
            <p>お知らせ</p>
        </div>
        <div class="c-information__content">
            2018年0月0日<br class="sp-only"/>　インドアゴルフスクール ARROWS 東松山店 サイト公開しました。
        </div>
    </div>
</section>
<!-- ＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊ -->
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?> 