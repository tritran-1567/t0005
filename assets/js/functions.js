/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}

$(document).ready(function() {  
	//scoll hide/show scoll-top
	var pageTop = $('.c-scroll');
	pageTop.hide();
	$(window).scroll(function () {
		if($(this).scrollTop() < 100){
			pageTop.fadeOut();
		} else{
			pageTop.fadeIn();
		}
	});
	// animation scroll top
	pageTop.click(function() {
	  $("html, body").animate({ scrollTop: 0 }, 1200);
	  return false;
  });
  
  // click hide/show c-box
  var boxInner = $('.c-box__inner');
  $('.c-box__title').on('click',function(){
    console.log($(this).parent());
    if($(this).parent().hasClass("active")){
      $(this).parent().removeClass("active");
      $(this).parent().find(boxInner).slideUp();
    }else{
      $(this).parent().addClass("active");
      $(this).parent().find(boxInner).slideDown();
    }
  });
});