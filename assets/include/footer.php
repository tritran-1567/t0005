<footer>
    <div class="c-footer">
        <a href="#" class="c-btn2">
            <img src="./assets/image/btn1-pc.png" alt="" class="pc-only">
            <img src="./assets/image/btn1-sp.png" alt="" width="240" class="sp-only">
        </a>
        <div class="c-footer__contact">
            〒355-0028 埼玉県東松山市箭弓町1-13-16 安福ビル1F　TEL.0493-23-8015<br/>
            営業時間：［平日］11:00〜21:00（昼休憩13:00〜14:00）／［土・日・祝］10:00〜19:00　<br class="sp-only"/>火曜定休<br/>
            <span class="pc-only">レッスン・フリー利用可能時間：会員・ビジター問わず1時間（準備時間含む）</span>
        </div>
    </div>
    <div class="c-footer__sub">
        Copyright (c) Shin Corporation All Rights Reserved.
    </div>
    <div class="c-scroll">
        <img src="./assets/image/scroll-pc.png" alt="">
    </div>
</footer>
<script src="/assets/js/functions.js"></script>
</body>
</html>